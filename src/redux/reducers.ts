import { actionTypes } from "./actions";
import { ReduxState } from "./types";

export const initialState: ReduxState = {
    tokenState: {
        token: '',
        isFetching: false,
        isError: false,
    },
}

function tokenReducer(state: ReduxState = initialState, action: any) {
    switch (action.type) {
        case actionTypes.FETCH_TOKEN:
            return {
                ...state,
                isFetching: true,
                isError: false,
            };
        case actionTypes.RECIEVE_TOKEN:
            return {
                ...state,
                token: action.data,
                isFetching: false,
                isError: false,
            };
        case actionTypes.RECIEVE_ERROR:
            return {
                ...state,
                isFetching: false,
                isError: true,
            };
        default:
            return state;
    }
}

export default tokenReducer;