export interface ReduxState {
    readonly tokenState: Token;
}

export interface Token {
    token: string;
    isFetching: boolean;
    isError: boolean;
}
