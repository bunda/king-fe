import { instance } from '../services/api';
import { AxiosResponse } from 'axios';

export enum actionTypes {
    FETCH_TOKEN = 'FETCH_TOKEN',
    RECIEVE_TOKEN = 'RECIEVE_TOKEN',
    RECIEVE_ERROR = 'RECIEVE_ERROR',
}

export const fetchToken = () => {
    return {
        type: actionTypes.FETCH_TOKEN,
    };
};

export const receiveToken = (token: string) => {
    return {
        type: actionTypes.RECIEVE_TOKEN,
        data: token,
    };
};

export const receiveError = () => {
    return {
        type: actionTypes.RECIEVE_ERROR,
    };
};

export const thunk_action_creator = () => {
    return function (dispatch: any, getState: any) {
        dispatch(fetchToken());

        instance.get('api/flights').then((res: AxiosResponse) => {
            if (res.status === 200) {
                dispatch(receiveToken(res.data));
            } else if (res.status === 204) {
                console.log("204: ", res.data);
                if (res.data) {
                    dispatch(receiveToken(res.data));
                }
            } else {
                console.log(`error status: ${res.status}\nerror text: ${res.statusText}`);
                dispatch(receiveError());

            }

        }).catch(err => {
            console.log({ err });
            dispatch(receiveError());

        });
    };
};
