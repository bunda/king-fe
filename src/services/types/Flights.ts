export interface SearchParamsModel {
    token: string;
    origin: string;
    destination: string;
    departureDate: Date;
    returnDate?: Date;
    currency?: string;
    adults?: number;
}

interface ExtendedSearchModel {
    departureNoOfStopover: number;
    returnNoOfStopover?: number;
    price: number;
}

export type FlightModel = SearchParamsModel & ExtendedSearchModel;

export default SearchParamsModel;
