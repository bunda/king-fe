import axios from 'axios';

export const instance = axios.create({
    baseURL: 'http://localhost:5010',
    timeout: 10000,
});

export const get = async <T>(url: string): Promise<T> => {
    const res = await instance.get<T>(url).catch((err: any) => {
        const { message, response } = err;
        console.log(err);
        if (message) return message;
        return response;
    });

    return res.data;
};

export const post = async (url: string, data: any): Promise<any> => {
    const res = await instance.post(url, data).catch((err: any) => {
        const { message, response } = err;
        console.log(err);
        if (message) return message;
        return response;
    });

    return res.data;
};
