import React from 'react';
import Layout from '../layout/Layout';
// import Button from 'antd/es/button';
import './App.css';

const App: React.FC = () => {
  return (
    <div className="App">
      <Layout />
    </div>
  );
}

export default App;
