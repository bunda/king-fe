import React from 'react';
import {
    Table,
} from 'antd';
import moment from 'moment';
import { FlightModel } from '../../services/types/Flights';
import './Flights.css';


interface Props {
    flights: FlightModel[];
}

const columns = [
    {
        title: 'Polazni aerodrom',
        dataIndex: 'origin',
        key: 'origin',
    },
    {
        title: 'Odredišni aerodrom',
        dataIndex: 'destination',
        key: 'destination',
    },
    {
        title: 'Datum polaska',
        dataIndex: 'departureDate',
        key: 'departureDate',
        render: (departureDate: Date) =>
            <p>{moment(departureDate).format('YYYY-MM-DD HH:mm:ss')}</p>,
    },
    {
        title: 'Datum odlaska',
        dataIndex: 'returnDate',
        key: 'returnDate',
        render: (returnDate?: Date) => returnDate
            ? <p>{moment(returnDate).format('YYYY-MM-DD HH:mm:ss')}</p>
            : <p>-</p>,
    },
    {
        title: 'Broj presjedanja - polazak',
        dataIndex: 'departureNoOfStopovers',
        key: 'departureNoOfStopovers',
    },
    {
        title: 'Broj presjedanja - odlazak',
        dataIndex: 'returnNoOfStopovers',
        key: 'returnNoOfStopovers',
        render: (num?: number) => num
            ? <p>{num}</p>
            : <p>-</p>
    },
    {
        title: 'Broj putnika',
        dataIndex: 'adults',
        key: 'adults',
    },
    {
        title: 'Cijena',
        key: 'price',
        render: (item: FlightModel) =>
            <p>{`${item.price} ${item.currency}`}</p>
    },
];

const Flights: React.FC<Props> = (props: Props) => {
    const { flights } = props;

    if (flights.length > 0) {
        console.log(moment(flights[0].departureDate));
    }

    return (
        <Table
            dataSource={flights}
            columns={columns}
            rowKey={(item: FlightModel) => `flight-${item}`}
        />
    );
}

export default Flights;
