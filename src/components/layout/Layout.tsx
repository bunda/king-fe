import React from 'react';
import { connect } from 'react-redux';
import {
    Layout as Layt, Spin,
} from 'antd';
import SidebarForm from '../sidebar/Sidebar';
import './Layout.css';
import { Token } from '../../redux/types';
import { thunk_action_creator } from '../../redux/actions';
import SearchParamsModel, { FlightModel } from '../../services/types/Flights';
import { post } from '../../services/api';
import Flights from '../flights-table/Flights';

const { Content, Footer, Sider } = Layt;

interface Props {
    getToken: () => void,
    tokenState: Token,
}

interface State {
    flights: FlightModel[];
    isFetchingFlights: boolean;
}

class Layout extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            flights: [],
            isFetchingFlights: false,
        }
    }
    componentDidMount() {
        const { getToken } = this.props;

        getToken();
    }

    fetchFlights = async (data: SearchParamsModel) => {
        this.setState({
            isFetchingFlights: true,
        });

        const res = await post('api/flights', data);

        this.setState({
            flights: res,
            isFetchingFlights: false,
        });
    }

    render() {
        const { flights, isFetchingFlights } = this.state;
        return (
            <Layt>
                <Sider
                    className="sider"
                >
                    <div className="logo" />
                    <SidebarForm
                        handleSubmit={this.fetchFlights}
                        disableSubmitButton={isFetchingFlights}
                    />
                </Sider>
                <Layt style={{ marginLeft: 30, marginRight: 30 }}>
                    <Content className="content" style={{ marginTop: '100px' }}>
                        <Spin tip="Loading..." spinning={isFetchingFlights}>
                            {flights && flights.length > 0
                                ? (<Flights
                                    flights={flights}
                                />)
                                : (<p>Nema rezultata</p>)}
                        </Spin>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>Fly with us</Footer>
                </Layt>
            </Layt>
        );
    }
}

const mapStateToProps = (state: Token) => ({ tokenState: state });
const mapDispatchToProps = (dispatch: any) => {
    return {
        getToken: () => dispatch(thunk_action_creator()),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Layout);
