import React from 'react';
import { connect } from 'react-redux';
import {
    Form,
    Input,
    DatePicker,
    InputNumber,
    Select,
    Button,
} from 'antd';
import './Sidebar.css';
import { FormComponentProps } from 'antd/lib/form';
import { Token } from '../../redux/types';
import SearchParamsModel from '../../services/types/Flights';

const { Option } = Select;

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

interface Props extends FormComponentProps {
    tokenState: Token;
    handleSubmit: (data: SearchParamsModel) => Promise<void>;
    disableSubmitButton: boolean;
}


class Sidebar extends React.Component<any, Props> {
    compareToDeparture = (rule: any, value: any, callback: any) => {
        const { form } = this.props;
        if (value && value === form.getFieldValue('departure')) {
            callback('Polazni i odredišni aerodromi ne mogu biti jednaki!');
        } else {
            callback();
        }
    };

    handleSubmit = (e: any) => {
        const {
            form: { validateFieldsAndScroll },
            tokenState: { token },
            handleSubmit,
        } = this.props;

        e.preventDefault();
        validateFieldsAndScroll(async (err: any, values: any) => {
            if (!err) {
                values = { ...values, token };
                handleSubmit(values);
            }
        });
    };

    render() {
        const {
            form: { getFieldDecorator },
            disableSubmitButton,
        } = this.props;

        return (
            <Form {...formItemLayout} onSubmit={this.handleSubmit} style={{ marginTop: 100 }}>
                <Form.Item label="Polazni aerodrom">
                    {getFieldDecorator('origin', {
                        rules: [
                            {
                                required: true,
                                message: 'Molimo unesite polazni aerodrom!',
                            },
                        ],
                    })(<Input style={{ width: '90%' }} />)}
                </Form.Item>
                <Form.Item label="Dolazni aerodrom">
                    {getFieldDecorator('destination', {
                        rules: [
                            {
                                required: true,
                                message: 'Molimo unesite odredišni aerodrom!',
                            },
                            {
                                validator: this.compareToDeparture,
                            },
                        ],
                    })(<Input style={{ width: '90%' }} />)}
                </Form.Item>
                <Form.Item label="Datum polaska">
                    {getFieldDecorator('departureDate', {
                        rules: [{ type: 'object', required: true, message: 'Unesite datum!' }],
                    })(<DatePicker />)}
                </Form.Item>
                <Form.Item label="Datum povratka">
                    {getFieldDecorator('returnDate', {
                        rules: [{ type: 'object' }],
                    })(<DatePicker />)}
                </Form.Item>
                <Form.Item {...formItemLayout} label="Broj putnika">
                    {getFieldDecorator('adults', {
                        rules: [{ type: 'number' }],
                    })(<InputNumber min={1} max={50} />)}
                </Form.Item>
                <Form.Item label="Valuta">
                    {getFieldDecorator('currency')(
                        <Select placeholder="Odaberite valutu" style={{ width: '90%' }}>
                            <Option value="USD">USD</Option>
                            <Option value="EUR">EUR</Option>
                            <Option value="HRK">HRK</Option>
                        </Select>,
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit" disabled={disableSubmitButton}>
                        Pretraži
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}
const mapStateToProps = (state: Token) => ({ tokenState: state });

const SidebarForm = connect(mapStateToProps)
    (Form.create<Props>({ name: 'sidebar' })(Sidebar));

export default SidebarForm;
